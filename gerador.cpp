#include <bits/stdc++.h>
#include <unistd.h>

using namespace std;

int main(int argc, char * argv[])
{
  if (argc != 2)
    {
      puts("Uso: ./gerador número");
      return 0;
    }
  
  FILE * f = fopen("palavrasordem.txt","r");
  assert(f);

  char * buff = NULL;
  size_t n = 0;


  vector<string> word;
  while (getline(&buff,&n,f) != -1)
    word.push_back(buff);

  free(buff);
  fclose(f);
  
  auto size = word.size();

  int qnt = atoi(argv[1]);
  
  set<int> usado;

  while (usado.size() != qnt)
    {
      int n = rand() % (size - 1);
      if (usado.find(n) == usado.end())
	usado.insert(n);
    }

  char nome[200];
  sprintf(nome,"palavras_%d.txt",qnt);

  FILE * nf = fopen(nome,"w");
  assert(nf);

  for (auto k:usado)
    fputs(word[k].c_str(),nf);

  fflush(nf);
  fsync(fileno(nf));
  
  char * buff_caso_teste;
  FILE * fteste = fopen("caso_teste.txt","r");
  assert(fteste);

  fseek(fteste, 0, SEEK_END);
  size_t teste_size = ftell(fteste);
  buff_caso_teste = (char*)malloc(sizeof(char)*teste_size);
  fseek(fteste, 0, SEEK_SET);



  fread(buff_caso_teste,sizeof(char) ,teste_size, fteste);
  fwrite(buff_caso_teste, sizeof(char) ,teste_size, nf);


  free(buff_caso_teste);
  fclose(fteste);
  fclose(nf);
  
  char comand[200];
  sprintf(comand,"time ./teste_abb < %s",nome);
  system(comand);
  
  


  return 0;
}
