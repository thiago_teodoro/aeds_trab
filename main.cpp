#include <bits/stdc++.h>

using namespace std;

#define for_range(i,a,b) for (int i = (a); i <= (b); i++)

template<class key_t,class map_t,bool _AVL = true>
class my_map
{
  struct no
  {
    key_t key;
    map_t mapped;
    no * l,*r; 			// Filhos esquerdo e direito, respectivamente
    int h;			// altura
  };

  no * root; 			// A raiz da nossa árvore
  int _comparacoes;		// Número de comparações feitas
  string _rotacao;		// Indica a rotação realizada na inserção ou remoção
  
  int height(no * n)
  {
    if (n == NULL)
      return 0;

    return n->h;
  }

  no * new_no(const key_t &key)
  {
    no * n = new no;
    n->key = key;
    n->l = n->r = NULL;
    n->h = 1;

    return n;
  }

  no * rr(no * y)
  {
    no * x = y->l;
    no * t2 = x->r;

    // Rotaciona
    x->r = y;
    y->l = t2;

    // Atualiza os pesos
    y->h = max(height(y->l),height(y->r)) + 1;
    x->h = max(height(x->l),height(x->r)) + 1;

    // Retorna a nova raíz
    return x;
  }

  no * ll(no * x)
  {
    no * y = x->r;
    no * t2 = y->l;

    // Rotaciona
    y->l = x;
    x->r = t2;

    // Atualiza as alturas
    x->h = max(height(x->l),height(x->r))+1;
    y->h = max(height(y->l),height(y->r))+1;

    // Retorna a nova raíz
    return y;
  }

  int get_balance(no * n)
  {
    if (n == NULL)
      return 0;

    return height(n->l) - height(n->r);
  }

  // Insere a chave key na subarvore enraizada por 'n' e retorna a nova raiz dessa subarvore.
  // O nó onde se encontrar a chave key será colocado em ptr.
  no * insert(no * n, const key_t &key,no ** ptr)
  {
    _comparacoes++;

    if (n == NULL)
      {
	*ptr = new_no(key);
	return *ptr;
      }

    if (key < n->key)
      n->l = insert(n->l, key,ptr);
    else if (key > n->key)
      n->r = insert(n->r, key,ptr);
    else
      return *ptr = n;

    // Se for AVL então fazemos o balanceamento.
    if (_AVL)
      {
	n->h = max(height(n->l),height(n->r)) + 1;
    
	int balance = get_balance(n);

	if (balance > 1)
	  {
	    if (key < n->l->key) // esquerda-esquerda
	      {
		_rotacao = "RR";
		return rr(n); 	
	      }
	    else			// esquerda-direita
	      {
		_rotacao = "LR";
		n->l = ll(n->l);
		return rr(n);
	      }
	  }
	else if (balance < -1)
	  {
	    if (key > n->r->key) 	// direita-direita
	      {
		_rotacao = "LL";
		return ll(n);
	      }
	    else			// direita-esquerda
	      {
		_rotacao = "RL";
		n->r = rr(n->r);
		return ll(n);
	      }
	  }
      }
    // Se o nó n não foi alterado
    return n;
  }

  // Retorna o nó com a menor chave na subárvore enraizada por n. (Nó mais a esquerda)
  no * min_value_no(no * n)
  {
    no * p = n;
    while (p->l)
      p = p->l;

    return p;
  }

  // Uma função que remove o nó com chave key da arvore enraizada por 'n', retornando
  // a raiz dessa árvore. 
  no * delete_no(no * n,const key_t &key)
  {
    _comparacoes++;
    
    if (n == NULL)
      return n;

    if (key < n->key) 		// A chave está na subarvore esquerda
      n->l = delete_no(n->l,key);
    else if (key > n->key) 	// A chave está na subarvore direita
      n->r = delete_no(n->r,key);
    else 			// Esse é o nó a ser removido
      {
	// Caso com um único filho
	if (n->l == NULL or n->r == NULL)
	  {
	    no * t = n->l ? n->l : n->r;

	    // Não possui filhos
	    if (t == NULL)
	      {
		t = n;
		n = NULL;
	      }
	    else
	      *n = *t; 		// Copia o conteúdo de seu filho.

	    delete t;
	  }
	// Tem dois filhos
	else
	  {
	    // Pegamos o sucessor do nó n. (O nó com a menor chave na subarvore direita do nó n)
	    no * t = min_value_no(n->r);

	    // Compiamos os dados do sucessor de n para n.
	    n->key = t->key;
	    n->mapped = t->mapped;

	    // Removemos esse sucessor
	    n->r = delete_no(n->r, t->key);
	  }
      }


    // Se a árvore tinha apenas um nó então retornamos.
    if (n == NULL)
      return n;

    // Se a árvore for AVL então fazemos o balanceamento.
    if (_AVL)
      {      
	n->h = 1 + max(height(n->l),height(n->r));

	int balance = get_balance(n);



	if (balance > 1)
	  {
	    // Caso esquerda-esquerda
	    if (get_balance(n->l) >= 0)
	      {
		_rotacao = "RR";
		return rr(n);
	      }
	    else 			// Caso esquerda-direita
	      {
		_rotacao = "LR";
		n->l = ll(n->l);
		return rr(n);
	      }
	  }

	if (balance < -1)
	  {
	    // Caso direita-direita
	    if (get_balance(n->r) <= 0)
	      {
		_rotacao = "LL";
		return ll(n);
	      }
	    else 			// Caso direita-esquerda
	      {
		_rotacao = "RL";
		n->r = rr(n->r);
		return ll(n);
	      }
	  }

      }

    return n;
  }
  
  
  // Imprime a subarvore enraizada por 'n' em ordem
  void _print_ordem(no * n)
  {
    if (n != NULL)
      {
	_print_ordem(n->l);
	cout << "(" << n->key << "," << n->mapped << ") ";
	_print_ordem(n->r);
      }
  }
  
public:
  my_map()
  {
    root = NULL;
  }

  map_t & operator[](const key_t &key)
  {
    _comparacoes = 0;
    _rotacao = "";
    no * ptr;
    root = insert(root, key, &ptr);
    return ptr->mapped;
  }

  void remove(const key_t &key)
  {
    _rotacao = "";
    _comparacoes = 0;
    root = delete_no(root, key);
  }
  
  void print_ordem()
  {
    _print_ordem(root);
    puts("");
  }

  // Verifica se a chave key existe na arvore
  bool exists(const key_t &key)
  {
    no * n = root;
    _comparacoes = 0;
    while (n)
      {
	_comparacoes++;
	if (n->key == key)
	  return true;
	else if (key < n->key)
	  n = n->l;
	else
	  n = n->r;
      }
    return false;
  }

  int comparacoes()
  {
    return _comparacoes;
  }

  string rotacao()
  {
    return _rotacao;
  }
  
};


int main()
{

  #ifdef ABB
  my_map<string,int,false> m;
  #else
  my_map<string,int> m;
  #endif

  for (;;)
    {
      string line;
      getline(cin,line);
      if (line == "0")
	break;
      
      for (auto &k:line)
	{
	  if (k == '.' or k == '?' or k == '!')
	    k = ' ';
	  else
	    k = tolower(k);
	}
      
      istringstream is(line);

      string buff;
      while (is >> buff)
	{
	  if (!m.exists(buff))
	    m[buff] = 1;
	  else
	    m[buff]++;
	}
    }

  bool primeiro = true;
  for (;;)
    {
      int n;
      scanf("%d",&n);

      if (n == 6)
  	break;


      if (!primeiro)
  	puts("");
      else
  	primeiro = false;

      char buff[200];
      if (n != 4)
      scanf("%s",buff);

      if (n == 1)
  	{
  	  if (m.exists(buff))
  	    {
  	      int comp = m.comparacoes();
  	      printf("Encontrou \"%s\" - frequência %d - com %d comparações\n",buff,m[buff],comp);
  	    }
	      
  	  else
  	    printf("Não encontrou \"%s\"\n",buff);
  	}
      else if (n == 2)
  	{
  	  if (m.exists(buff))
  	      m[buff]++;
  	  else
  	      m[buff] = 1;

  	  string r = m.rotacao();
  	  int comp = m.comparacoes();
  	  if (r.empty())
  	    printf("Inseriu \"%s\" - frequência %d - com %d comparações\n",buff,m[buff],comp);
  	  else
  	    printf("Inseriu \"%s\" - frequência %d - com %d comparações + rotação %s\n",buff,m[buff],comp
  		   ,r.c_str());
  	}
      else if (n == 3)
  	{
  	  if (m.exists(buff))
  	    {
  	      int f = m[buff];
  	      m.remove(buff);

  	      string r = m.rotacao();
  	      int comp = m.comparacoes();
  	      if (r.empty())
  		printf("Removeu \"%s\" - frequência %d - com %d comparações\n",buff,f,comp);
  	      else
  		printf("Removeu \"%s\" - frequência %d - com %d comparações + rotação %s\n",buff,f,comp
  		       ,r.c_str());
  	    }
  	  else
  	    printf("Impossível remover \"%s\" - não se encontra na árvore\n",buff);
  	}
      else if (n == 4)
	{
	  printf("Impressão em ordem: ");
	  m.print_ordem();
	}

    }

  return 0;
}



