all: teste_avl teste_abb

teste_avl: main.cpp
	g++ -Wall -Wextra -O0 -g3  main.cpp -o teste_avl

teste_abb: main.cpp
	g++ -D ABB -Wall -Wextra -O0 -g3  main.cpp -o teste_abb
